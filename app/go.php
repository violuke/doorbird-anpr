<?php

require __DIR__.'/../vendor/autoload.php';

echo 'Beginning'."\n";

// To run me!
// docker-compose run app php /data/app/go.php
// docker-compose run openalpr -c gb images/1531243505.jpg

$doorbirdANPR = new DoorbirdANPR();
$doorbirdANPR->go();

class DoorbirdANPR {
    private $env = [];

    public function go(){
        $this->env = require __DIR__.'/env.php';

        while (true) {
            echo 'Retrieving latest image...' . "\n";

            try {
                $imageFile = $this->getImageFromDoorbirdRTSP();
//                $imageFile = $this->getImageFromDoorbirdVideoAPI();
            } catch (Exception $e){
                echo 'Caught exception, so sleeping for 5 seconds: '.$e->getMessage()."\n";
            }

            if ($this->imageShowsValidPlate($imageFile)) {
                echo 'Opening gate...' . "\n";
                $this->openGate();

                echo 'As we have just opened the gate, sleep for 20 seconds...'."\n";
                sleep(20);
            } else {
                echo 'Plate not found' . "\n";
            }

//            $this->deleteImage($imageFile);

            echo 'Sleeping for a moment...' . "\n";
            sleep(2);
            exit;
        }
    }

    private function getImageFromDoorbirdRTSP(): string {
        $filename = __DIR__.'/../images/'.time().'.jpg';
        shell_exec('ffmpeg -rtsp_transport tcp -y -i '.escapeshellarg('rtsp://'.urlencode($this->env['doorBirdUser']).':'.urlencode($this->env['doorBirdPassword']).'@'.$this->env['doorBirdHost'].':554/mpeg/media.amp').' -vframes 1 '.escapeshellarg($filename));

        echo 'Saved image to '.$filename."\n";
        //echo shell_exec('ls -alh '.__DIR__);
        //echo shell_exec('ls -alh '.__DIR__.'/..');
        //echo shell_exec('ls -alh '.__DIR__.'/../images');

        return $filename;
    }

    private function imageShowsValidPlate(string $filename): bool {
        // TODO: For testing, remove me
//        $filename = __DIR__.'/../images/tester3.jpg';

        $command = 'alpr -c gb --debug --config '.escapeshellarg(__DIR__.'/alpr_config.conf').' '.escapeshellarg($filename);
        echo 'Checking for plates: '.$command."\n";
        $result = shell_exec($command);

        echo 'Result: '.$result."\n";

        if (preg_match_all('/- ([A-Z0-9]+)\s+confidence: ([0-9.]+)/', $result, $matches)){
            foreach ($matches[1] as $plate){
                echo 'Testing plate '.$plate."\n";
                if (in_array($plate, $this->env['trustedPlates'])){
                    echo 'Exact match on plate '.$plate."\n";
                    return true;
                } else {
                    foreach ($this->env['trustedPlates'] as $validPlate){
                        if (levenshtein($validPlate, $plate) < 2){
                            echo 'levenshtein match on plate '.$plate. ' and the valid plate of '.$validPlate."\n";
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    private function openGate(){
        $client = new GuzzleHttp\Client();
        $client->request('GET', 'http://'.$this->env['doorBirdHost'].'/bha-api/open-door.cgi', [
            'auth' => [$this->env['doorBirdUser'], $this->env['doorBirdPassword']]
        ]);
    }
    
    
    private function deleteImage(string $filename){
        unlink($filename);
    }
    


    private function getImageFromDoorbirdImageAPI(){
        $client = new GuzzleHttp\Client();

        $res = $client->request('GET', $this->env['doorBirdHost'].'/bha-api/image.cgi?resolution=320x320', [
            'auth' => [$this->env['doorBirdUser'], $this->env['doorBirdPassword']]
        ]);

        file_put_contents(__DIR__.'/../images/'.time().'.txt', $res->getBody());
    }

    private function getImageFromDoorbirdVideoAPI(){
        $camurl = 'http://'.$this->env['doorBirdHost'].'/bha-api/video.cgi?http-user='.urlencode($this->env['doorBirdUser']).'&http-password='.urlencode($this->env['doorBirdPassword']);

        $boundary = '--my-boundary';                                   // $boundary = The boundary string between jpegs in an mjpeg stream

        $f = fopen($camurl, 'r');                                       // Open mjpeg url as $f in readonly mode
        $r = '';                                                        // Set $r to blank variable

        $imageFile = __DIR__.'/../images/'.time().'.jpg';

        if($f) {
            while (substr_count($r,'Content-Length') < 2) {           // While the number of times 'Content-Length' appears in $r is less than 2
                $r .= fread($f, 16384); // Append 16384 bytes of $f to $r
            }

            $start = strpos($r, $boundary);                           // $start is set to the position of the first occurrence of '$boundary' in $r
            $end = strpos($r, $boundary, $start + strlen($boundary)); // $end is essentially set to the position of the second occurrence of '$boundary' in $r. I use $start + strlen($boundary) to offset the start position and skip the first occurrence
            $boundaryAndFrame = substr($r, $start, $end - $start);    // $boundaryAndframe is set to the string in $r starting at position $start and with a length of ($end - $start)

            $pattern='/(Content-Length:\s*\d*\r\n\r\n)([\s\S]*)/';      // Use regex to search for '(Content-Length:       90777\r\n\r\n)(<jpeg image data>)
            preg_match_all($pattern, $boundaryAndFrame, $matches);      // Search for regex matches in $boundaryAndFrame
            $frame = $matches[2][0];                                    // $frame is set to the second regex character group (in this case, <jpeg image data>)

            file_put_contents($imageFile, $frame);

        } else {
            throw new Exception('Error, cannot open URL');
        }

        fclose($f);

        echo 'Saved image: '.$imageFile."\n";

        return $imageFile;
    }

}