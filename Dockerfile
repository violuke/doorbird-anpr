# FROM php:7.2-fpm
FROM openalpr

MAINTAINER Luke Cousins

RUN apt-get update -yqq
RUN apt-get install -yqq ffmpeg php7.2-cli
RUN rm -r /var/lib/apt/lists/*

COPY . /doorbird-anpr
#VOLUME ["/data"]

ENTRYPOINT ["php", "/doorbird-anpr/app/go.php"]