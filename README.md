# doorbird-anpr

A project attempting to automatically open our gates based on vehicle number plate recognition. Doorbird is used as both gate relay controller and image source.

**Currently incomplete, don't use it yet**

Vendor files are included to save the hassle of composer on devices like Raspberry Pi's which may be running this code

## All in one Dockefile
```bash
# Create your config
nano app/env.php

docker build -t openalpr https://github.com/openalpr/openalpr.git
docker build -t doorbird-anpr .
docker run -it doorbird-anpr

# or

docker run -v "$(pwd):/doorbird-anpr" doorbird-anpr
```


## Agent
```bash
# First run, to register
docker run -v "$(pwd)/agent/config:/etc/openalpr/" -v "$(pwd)/agent/images:/var/lib/openalpr/" -it --expose 11300 --expose 8355 "openalpr/commercial-agent" alprlink-register

# Subsequent runs
docker run --restart always -d -P -v "$(pwd)/agent/config:/etc/openalpr/" -v "$(pwd)/agent/images:/var/lib/openalpr/" -it "openalpr/commercial-agent"
```

## Simple run
```bash
docker run -it --rm -v "$(pwd)/agent/config:/etc/openalpr/" -v $(pwd):/data:ro openalpr/openalpr -c gb 1531418412.jpg
docker run --rm -ti --entrypoint=openalpr-utils-calibrate -e DISPLAY -v "$(pwd):/data:ro" --ipc=host -v "$PWD:/openalpr" openalpr/openalpr -c gb 1531418412.jpg
docker run --rm -ti --entrypoint=openalpr-utils-calibrate -e DISPLAY -v "$(pwd):/data:ro" --ipc=host -v "$PWD:/openalpr" openalpr -c gb 1531418412.jpg

docker run --rm -ti --entrypoint=openalpr-utils-calibrate -e DISPLAY -v /tmp:/tmp --ipc=host -v "$PWD:/openalpr" wallneradam/docker-openalpr-alpine -c gb 1531418412.jpg




docker run -it --rm -v "$(pwd)/agent/config:/etc/openalpr/" -v "$(pwd)/images:/data:ro" openalpr/openalpr -c gb 1531418412.jpg
```